# Myd

Pronounced /mi:d/, like mead

`Myd` is a rust library to create a representation of the
rust module tree as a usable datatype, and to resolve imports. It
is designed to be used by tools working with rust source code to
be less naïve about the module system.

## Examples

Parse a package:

```rust
// Expensive operation: may take a few seconds
let mi = myd::parse::parse("./Cargo.toml");
```

Work out where something is imported from:

```rust
let my_path: syn::Path = syn::parse_str("std::collections::HashMap");
let hashmap = mi.path(my_node, my_path)
```

See the [examples](https://gitlab.com/john_t/myd/-/tree/imports/examples) for more.
