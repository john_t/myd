use myd::{platforms::SystemInfo, CargoOpt, Module};
use proc_macro2::{Ident, Span};

macro_rules! path {
    ($pth:path) => {
        syn::parse_str::<syn::Path>(stringify!($pth)).unwrap()
    };
}

fn ident(s: &str) -> Ident {
    Ident::new(s, Span::call_site())
}

#[test]
fn imports() -> Result<(), Box<dyn std::error::Error>> {
    let mi = myd::parse::parse(
        "tests/test_folder/Cargo.toml",
        CargoOpt::AllFeatures,
        &SystemInfo::default(),
    )?;
    println!("Parsed");
    let project = mi.tree.root().first_child().unwrap();
    let a_mod = project.first_child().unwrap();
    let a_mod_foo = Module::get_item(a_mod, &mi, &ident("Foo"))?.unwrap();
    let a_mod_afoo = Module::get_item(a_mod, &mi, &ident("AFoo"))?.unwrap();
    let b_mod = project.first_child().unwrap().next_sibling().unwrap();
    let b_mod_foo = Module::get_item(b_mod, &mi, &ident("Foo"))?.unwrap();
    let b_mod_bar = Module::get_item(b_mod, &mi, &ident("Bar"))?.unwrap();
    let d_mod = mi.path(project.id(), &path!(d))?.unwrap_module();
    assert_eq!(mi.path(project.id(), &path!(a::Foo))?, a_mod_foo);
    assert_eq!(mi.path(project.id(), &path!(b::Foo))?, b_mod_foo);
    assert_eq!(mi.path(project.id(), &path!(b::AFoo))?, a_mod_afoo);
    assert_eq!(
        mi.path(d_mod, &path!(crate::a))?.unwrap_module(),
        a_mod.id()
    );
    assert_eq!(mi.path(d_mod, &path!(Foo))?, a_mod_foo,);
    assert_eq!(mi.path(d_mod, &path!(Bar))?, b_mod_bar,);
    assert!(mi.path(project.id(), &path!(c::Foo)).is_err());

    Ok(())
}
