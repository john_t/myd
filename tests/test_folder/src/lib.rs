fn main() {
    b::Foo;
}

mod a {
    pub struct Foo;
    pub struct Bar;
    pub struct AFoo;
}

mod b {
    pub use super::a::*;
    pub struct Foo;
    pub struct Bar;
    pub struct BFoo;
}

mod c {
    use super::a::*;
    use super::b::*;
}

mod d {
    pub use super::a::Foo;
    pub use crate::b::Bar;
}
