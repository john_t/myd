//! A module to allow faked packages
//!
//! This is because the types in `cargo_metadata` are `#[non_exhuastive]`
//! meaning that we are unable to create our own.

use std::{borrow::Cow, path::Path};

use cargo_metadata::{Edition, Package};

/// A simlpe alias arround `Cow<'a, str>`
type CowStr<'a> = Cow<'a, str>;

/// A package that is represented manually,
/// not from `cargo_metadata`
pub struct ManualPackage<'a> {
    /// The name of our package
    pub name: CowStr<'a>,
    /// The rust edition of our package
    pub edition: Option<Edition>,
    /// The targets provided by our package
    pub targets: Vec<ManualTarget<'a>>,
}

impl<'a> ManualPackage<'a> {
    /// Creates a new rustlib manual package
    pub fn new_rl(name: &'a str, src_path: &'a Path) -> Self {
        Self {
            name: name.into(),
            edition: None,
            targets: vec![ManualTarget {
                kind: vec![String::from("lib")],
                src_path: Cow::Borrowed(src_path),
            }],
        }
    }
}

impl From<Package> for ManualPackage<'static> {
    fn from(them: Package) -> Self {
        Self {
            name: them.name.into(),
            edition: Some(them.edition),
            targets: {
                let mut vec = Vec::new();
                for target in them.targets {
                    vec.push(ManualTarget {
                        kind: target.kind,
                        src_path: target.src_path.into(),
                    })
                }
                vec
            },
        }
    }
}

#[derive(Clone)]
/// A target (`lib`, `bin`, &c)
pub struct ManualTarget<'a> {
    /// The kind, e.g. `lib`
    pub kind: Vec<String>,
    /// The path to the source code
    pub src_path: Cow<'a, Path>,
}
