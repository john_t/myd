//! Some internal helpers for working with syn

use proc_macro2::Span;
use syn::{Ident, Item, Visibility};

/// Gets the identifier from an item, or `None` if not present.
///
/// This will not work with everything, such as macro v2s
pub fn item_ident(i: &Item) -> Option<&Ident> {
    match i {
        Item::Const(x) => Some(&x.ident),
        Item::Enum(x) => Some(&x.ident),
        Item::ExternCrate(x) => Some(&x.ident),
        Item::Fn(x) => Some(&x.sig.ident),
        Item::ForeignMod(_) => None,
        Item::Impl(_) => None,
        Item::Macro(_) => None,
        Item::Mod(x) => Some(&x.ident),
        Item::Static(x) => Some(&x.ident),
        Item::Struct(x) => Some(&x.ident),
        Item::Trait(x) => Some(&x.ident),
        Item::TraitAlias(x) => Some(&x.ident),
        Item::Type(x) => Some(&x.ident),
        Item::Union(x) => Some(&x.ident),
        Item::Use(_) => None,
        Item::Verbatim(_) => None,
        _ => None,
    }
}

/// Gets the visibility of an item if present
pub fn item_vis(i: &Item) -> Option<&Visibility> {
    match i {
        Item::Const(expr) => Some(&expr.vis),
        Item::Enum(expr) => Some(&expr.vis),
        Item::ExternCrate(expr) => Some(&expr.vis),
        Item::Fn(expr) => Some(&expr.vis),
        Item::ForeignMod(_) => None,
        Item::Impl(_) => None,
        Item::Macro(_) => None,
        Item::Mod(expr) => Some(&expr.vis),
        Item::Static(expr) => Some(&expr.vis),
        Item::Struct(expr) => Some(&expr.vis),
        Item::Trait(expr) => Some(&expr.vis),
        Item::TraitAlias(expr) => Some(&expr.vis),
        Item::Type(expr) => Some(&expr.vis),
        Item::Union(expr) => Some(&expr.vis),
        Item::Use(expr) => Some(&expr.vis),
        Item::Verbatim(_) => None,
        _ => todo!(),
    }
}

/// Returns an identifier of `self`
pub fn ident_self() -> Ident {
    Ident::new("self", Span::call_site())
}

/// Returns an identifier of `super`
pub fn ident_super() -> Ident {
    Ident::new("super", Span::call_site())
}

/// Returns an identifier of `crate`
pub fn ident_crate() -> Ident {
    Ident::new("crate", Span::call_site())
}
