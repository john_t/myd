use ego_tree::{NodeId, NodeRef};
use myd::{platforms::SystemInfo, CargoOpt, Module};
use std::collections::HashMap;
use syn::Visibility;

pub fn main() {
    let sysinfo = SystemInfo::new_from_rustup().unwrap();
    let p = match myd::parse::parse("./Cargo.toml", CargoOpt::AllFeatures, &sysinfo) {
        Ok(p) => p,
        Err(e) => {
            eprintln!("{e}");
            return;
        }
    };

    let mut map = HashMap::new();
    println!("digraph {{");
    println!("\tnode[style=filled fontfamily = \"Fira Sans Heavy, sans\"]");
    println!("\tlayout = circo");
    for node in p.tree.nodes() {
        let len = map.len();
        let id = *map.entry(node.id()).or_insert(len);
        println!(
            "\tf{id} [ label = \"{}\", fillcolor = \"{}\"]",
            node.value().ident,
            if let Visibility::Public(_) = &node.value().vis {
                "#0000FF"
            } else {
                "#FFFF00"
            }
        )
    }
    iterate_over_nodes(p.tree.root(), &mut map);
    println!("}}");
}

fn iterate_over_nodes(p: NodeRef<Module>, map: &mut HashMap<NodeId, usize>) {
    let len = map.len();
    let id = *map.entry(p.id()).or_insert(len);
    for node in p.children() {
        let len = map.len();
        let child_id = *map.entry(node.id()).or_insert(len);
        if id != 0 {
            println!("\tf{id} -> f{child_id}");
        }
        iterate_over_nodes(node, map);
    }
}
